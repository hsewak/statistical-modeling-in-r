---
title: 'STAT 420: HW 4'
author: "Heather Sewak"
date: "September 17, 2016"
output: html_document
---

```{r setup, echo = FALSE, message = FALSE, warning = FALSE}
options(scipen = 1, digits = 4, width = 80)
```

# Assignment

## Exercise 1 (Using `lm` for Inference)

For this exercise we will again use the `faithful` dataset. Remember, this is a default dataset in `R`, so there is no need to load it. You should use `?faithful` to refresh your memory about the background of this dataset about the duration and waiting times of eruptions of [the Old Faithful geyser] in [Yellowstone National Park].

**(a)** Fit the following simple linear regression model in `R`. Use the eruption duration as the response and waiting time as the predictor. 

\[
Y_i = \beta_0 + \beta_1 x_i + \epsilon_i
\]

Store the results in a variable called `faithful_model`. Use a $t$ test to test the significance of the regression. Report the following:

- The null and alternative hypotheses
- The value of the test statistic
- The p-value of the test
- A statistical decision at $\alpha = 0.01$
- A conclusion in the context of the problem

When reporting these, you should explicitly state them in your document, not assume that a reader will find and interpret them from a large block of `R` output.
```{r eval=TRUE}
faithful_model = lm(eruptions ~ waiting, data=faithful)
summary(faithful_model)
```
Null and Alternative Hypothesis:
H0:B1 = 0 vs. H1: B1 != 0

```{r eval=TRUE}
(B1_hat_t = summary(faithful_model)$coefficients[2,3])
(B1_hat_pval = summary(faithful_model)$coefficients[2,4])

t.test(faithful$waiting, faithful$eruptions, conf.level = .99)
```
As shown by the results of the T test, the alternative hypothesis is true. Considering that the p-value is so low, the null hypothesis is rejected.

**(b)** Calculate a 99% confidence interval for $\beta_1$. Give an interpretation of the interval in the context of the problem.
```{r eval=TRUE}
confint(faithful_model, level = 0.99)[2,]
```
99% confident that for an increase in waiting time of 1 minute, the average increase in eruption minutes is between 0.0698727 and 0.0813832. Also, proves that the alternative hypothesis is true.

**(c)** Calculate a 90% confidence interval for $\beta_0$. Give an interpretation of the interval in the context of the problem.
```{r eval=TRUE}
confint(faithful_model, level = 0.90)[1,]
```
The confidence interval for beta_0 is within a negative range. If the waiting duration is 0 minutes, the eruption durations would be between -2.13833519 and -1.60969678. The eruption duration cannot be negative figures.

**(d)** Use a 95% confidence interval to estimate the mean eruption duration for waiting times of 75 and 80 minutes. Which of the two intervals is wider? Why?
```{r eval=TRUE}
new_waiting = data.frame(waiting = c(75, 80))
predict(faithful_model, newdata = new_waiting, 
        interval = c("confidence"), level = 0.95)
```
The 80 minute interval is wider because the higher the wait time the higher the eruption duration.

**(e)** Use a 95% prediction interval to predict the eruption duration for waiting times of 75 and 100 minutes.
```{r eval=TRUE}
new_waiting = data.frame(waiting = c(75, 100))
predict(faithful_model, newdata = new_waiting, 
        interval = c("prediction"), level = 0.95)
```

**(f)** Create a scatterplot of the data. Add the regression line, 95% confidence bands, and 95% prediction bands.
```{r eval=TRUE}
wait_grid = seq(min(faithful$waiting), max(faithful$waiting), by = 0.01)
dur_ci_band = predict(faithful_model, 
                           newdata = data.frame(waiting = wait_grid), 
                           interval = "confidence", level = 0.95)
dur_pi_band = predict(faithful_model, 
                           newdata = data.frame(waiting = wait_grid), 
                           interval = "prediction", level = 0.95) 

plot(eruptions ~ waiting, data = faithful,
     xlab = "Wait (Mins)",
     ylab = "Eruption Duration (Mins)",
     main = "Wait vs. Eruption",
     pch  = 20,
     cex  = 2,
     col  = "dodgerblue",
     ylim = c(0, 10))
abline(faithful_model, lwd = 5, col = "navy")

lines(wait_grid, dur_ci_band[,"lwr"], col = "red", lwd = 3, lty = 2)
lines(wait_grid, dur_ci_band[,"upr"], col = "red", lwd = 3, lty = 2)
lines(wait_grid, dur_pi_band[,"lwr"], col = "gold", lwd = 3, lty = 3)
lines(wait_grid, dur_pi_band[,"upr"], col = "gold", lwd = 3, lty = 3)
points(mean(faithful$waiting), mean(faithful$eruptions), pch = "+", cex = 3)
```

## Exercise 2 (Using `lm` for Inference)

For this exercise we will again use the `diabetes` dataset, which can be found in the `faraway` package.

**(a)** Fit the following simple linear regression model in `R`. Use the total cholesterol as the response and weight as the predictor. 

\[
Y_i = \beta_0 + \beta_1 x_i + \epsilon_i
\]

Store the results in a variable called `cholesterol_model`. Use an $F$ test to test the significance of the regression. Report the following:

- The null and alternative hypotheses
- The ANOVA table (You may use `anova()` and omit the row for Total.)
- The value of the test statistic
- The p-value of the test
- A statistical decision at $\alpha = 0.05$
- A conclusion in the context of the problem

When reporting these, you should explicitly state them in your document, not assume that a reader will find and interpret them from a large block of `R` output.
```{r eval=TRUE}
library(faraway)
cholesterol_model = lm(chol ~ weight, data=diabetes)
summary(cholesterol_model)
```
Null and Alternative Hypothesis:
H0:B1 = 0 vs. H1: B1 != 0

```{r eval=TRUE}
anova(cholesterol_model)
(B1_hat_t = summary(cholesterol_model)$coefficients[2,3])
(B1_hat_pval = summary(cholesterol_model)$coefficients[2,4])
t.test(diabetes$weight, diabetes$chol, conf.level = .95)
```

As shown by the results of the T test, the alternative hypothesis is true. Considering that the p-value is so low, the null hypothesis is rejected.

**(b)** Fit the following simple linear regression model in `R`. Use HDL as the response and weight as the predictor. 

\[
Y_i = \beta_0 + \beta_1 x_i + \epsilon_i
\]

Store the results in a variable called `hdl_model`. Use an $F$ test to test the significance of the regression. Report the following:

- The null and alternative hypotheses
- The ANOVA table (You may use `anova()` and omit the row for Total.)
- The value of the test statistic
- The p-value of the test
- A statistical decision at $\alpha = 0.05$
- A conclusion in the context of the problem

When reporting these, you should explicitly state them in your document, not assume that a reader will find and interpret them from a large block of `R` output.
```{r eval=TRUE}

hdl_model = lm(hdl ~ weight, data=diabetes)
summary(hdl_model)
```
Null and Alternative Hypothesis:
H0:B1 = 0 vs. H1: B1 != 0

```{r eval=TRUE}
anova(hdl_model)
(B1_hat_t = summary(hdl_model)$coefficients[2,3])
(B1_hat_pval = summary(hdl_model)$coefficients[2,4])
t.test(diabetes$weight, diabetes$hdl, conf.level = .95)
```

The alternative hypothesis is true since the p-value is so low.

## Exercise 3 (Inference "without" `lm`)

For this exercise we will once again use the data stored in [`goalies.csv`](goalies.csv). It contains career data for all 716 players in the history of the National Hockey League to play goaltender through the 2014-2015 season. The two variables we are interested in are:

- `W` - Wins
- `MIN` - Minutes

Fit a SLR model with `W` as the response and `MIN` as the predictor. Test $H_0: \beta_1 = 0.008$ vs $H_1: \beta_1 < 0.008$ at $\alpha = 0.01$. Report the following: 

- $\hat{\beta_1}$
- $SE[\hat{\beta_1}]$
- The value of the $t$ test statistic
- The degrees of freedom
- The p-value of the test
- A statistical decision at $\alpha = 0.01$

When reporting these, you should explicitly state them in your document, not assume that a reader will find and interpret them from a large block of `R` output.

You should use `lm()` to fit the model and obtain the estimate and standard error. But then you should directly calculate the remaining values. Hint: be careful with the degrees of freedom. Think about how many observations are being used.

```{r eval=TRUE}
goalies = read.csv("goalies.csv")
goalies_model = lm(W ~ MIN, data = goalies)
summary(goalies_model)
```

```{r eval=TRUE}
(B1_hat_est = summary(goalies_model)$coefficients[2,1])
(B1_hat_SE = summary(goalies_model)$coefficients[2,2])
(B1_t = (B1_hat_est - 0.008) / B1_hat_SE)
(B1_df = length(resid(goalies_model)) - 2)
(B1_pval = 2 * pt(abs(B1_t), df = B1_df, lower.tail = FALSE))
t.test(goalies$MIN, goalies$W, conf.level = .99)
```

Null hypothesis is rejected.

## Exercise 4 (Simulating Sampling Distributions)

For this exercise we will simulate data from the following model:

\[
Y_i = \beta_0 + \beta_1 x_i + \epsilon_i
\]

Where $\epsilon_i \sim N(0, \sigma^2).$ Also, the parameters are known to be:

- $\beta_0 = 4$
- $\beta_1 = 0.5$
- $\sigma^2 = 25$

We will use samples of size $n = 50$.

**(a)** Simulate this model $1500$ times. Each time use `lm()` to fit a SLR model, then store the value of $\hat{\beta}_0$ and $\hat{\beta}_1$. Set a seed using **your** UIN before performing the simulation. Note, we are simulating the $x$ values once, and then they remain fixed for the remainder of the exercise.

```{r eval=TRUE}
uin = 650039516
set.seed(uin)
n = 50
x = seq(0, 20, length = n)
Sxx = sum((x - mean(x)) ^ 2)
beta_0 = 4
beta_1 = 0.5
sigma = 5
beta_hat_0 = rep(0, 1500)
beta_hat_1 = rep(0, 1500)

sim_slr = function(n, x, beta_0 = 10, beta_1 = 5, sigma = 1) {
  epsilon = rnorm(n, mean = 0, sd = sigma)
  y       = beta_0 + beta_1 * x + epsilon
  data.frame(predictor = x, response = y)
}

for(i in 1:1500) {
  sim_data = sim_slr(n = 50, x = x, beta_0 = 4, beta_1 = 0.5, sigma = 5)
  model = lm(response ~ predictor, data = sim_data)
  beta_hat_0[i] = coef(model)[1]
  beta_hat_1[i] = coef(model)[2]
}
```

**(b)** For the *known* values of $x$, what is the expected value of $\hat{\beta}_1$?
```{r eval=TRUE}
(var_beta_1_hat = sigma ^ 2 / Sxx)
```

**(c)** For the known values of $x$, what is the standard deviation of $\hat{\beta}_1$?
```{r eval=TRUE}
(sd_beta_1_hat = sqrt(var_beta_1_hat))
```

**(d)** What is the mean of your simulated values of $\hat{\beta}_1$? Does this make sense given your answer in **(b)**?
```{r eval=TRUE}
(B1_hat_mean = mean(beta_hat_1))
```
The mean of 0.5031 makes sense as it it close to the true mean on beta_1.

**(e)** What is the standard deviation of your simulated values of $\hat{\beta}_1$? Does this make sense given your answer in **(c)**?
```{r eval=TRUE}
(B1_hat_sd = sd(beta_hat_1))
```
Yes, this makes sense as it is very close to the answer in "c".

**(f)** For the known values of $x$, what is the expected value of $\hat{\beta}_0$?
```{r eval=TRUE}
(var_beta_0_hat = sigma ^ 2 * (1 / n + mean(x) ^ 2 / Sxx))
```

**(g)** For the known values of $x$, what is the standard deviation of $\hat{\beta}_0$?
```{r eval=TRUE }
(sd_beta_0_hat = sqrt(var_beta_0_hat))
```

**(h)** What is the mean of your simulated values of $\hat{\beta}_0$? Does this make sense given your answer in **(f)**?
```{r eval=TRUE}
(B0_hat_mean = mean(beta_hat_0))
```
Yes, this empiricle mean is close to the true mean of 4 for beta_0.

**(i)** What is the standard deviation of your simulated values of $\hat{\beta}_0$? Does this make sense given your answer in **(g)**?
```{r eval=TRUE}
(B0_hat_sd = sd(beta_hat_0))
```
Yes, the values are closely related.

**(j)** Plot a histogram of your simulated values for $\hat{\beta}_1$. Add the normal curve for the true sampling distribution of $\hat{\beta}_1$.

```{r eval=TRUE}
hist(beta_hat_1, prob = TRUE, breaks = 20, 
     xlab = expression(hat(beta)[1]), main = "", col = "navy")
curve(dnorm(x, mean = beta_1, sd = sqrt(var_beta_1_hat)), 
      col = "darkorange", add = TRUE, lwd = 3)
```

**(k)** Plot a histogram of your simulated values for $\hat{\beta}_0$. Add the normal curve for the true sampling distribution of $\hat{\beta}_0$.
```{r eval=TRUE}
hist(beta_hat_0, prob = TRUE, breaks = 20, 
     xlab = expression(hat(beta)[0]), main = "", col = "gold")
curve(dnorm(x, mean = beta_0, sd = sqrt(var_beta_0_hat)), 
      col = "purple", add = TRUE, lwd = 3)
```

## Exercise 5 (Simulating Confidence Intervals)

For this exercise we will simulate data from the following model:

\[
Y_i = \beta_0 + \beta_1 x_i + \epsilon_i
\]

Where $\epsilon_i \sim N(0, \sigma^2).$ Also, the parameters are known to be:

- $\beta_0 = 1$
- $\beta_1 = 3$
- $\sigma^2 = 16$

We will use samples of size $n = 20$.

Our goal here is to use simulation to verify that the confidence intervals really do have their stated confidence level.

**(a)** Simulate this model $2000$ times. Each time use `lm()` to fit a SLR model, then store the value of $\hat{\beta}_0$ and $s_e$. Set a seed using **your** UIN before performing the simulation. Note, we are simulating the $x$ values once, and then they remain fixed for the remainder of the exercise.

```{r eval=TRUE}
uin = 650039516
set.seed(uin)
n = 20
x = seq(-5, 5, length = n)

Sxx = sum((x - mean(x)) ^ 2)
beta_0 = 1
beta_1 = 3
sigma = 4
beta_hat_0 = rep(0, 2000)
s_e = rep(0, 2000)
lower_90 = rep(0, 2000)
upper_90 = rep(0, 2000)
lower_99 = rep(0, 2000)
upper_99 = rep(0, 2000)


for(i in 1:2000) {
  eps = rnorm(n, mean = 0, sd = sigma)
  y   = beta_0 + beta_1 * x + eps
  model = lm(y ~ x)
  beta_hat_0[i] = coef(model)[1]
  y_hat = model$coefficients[1] + model$coefficients[2] * x
  e = y - y_hat
  s2_e = sum(e^2) / (n-2)
  s_e[i] = sqrt(s2_e)

  
  crit_90 = qt(0.90, df=n)
  margin_90 = s_e[i] * qt(0.95, df=n)
  lower_90[i] = beta_hat_0[i] - margin_90
  upper_90[i] = beta_hat_0[i] + margin_90
  
  crit_99= qt(0.99, df=n)
  margin_99 = s_e[i] * qt(0.99, df=n)
  lower_99[i] = beta_hat_0[i] - margin_99
  upper_99[i] = beta_hat_0[i] + margin_99
  
}
```

**(b)** For each of the $\hat{\beta}_0$ that you simulated calculate a 90% confidence interval. Store the lower limits in a vector `lower_90` and the upper limits in a vector `upper_90`. Some hints:

- You will need to use `qt()` to calculate the critical value, which will be the same for each interval.
- Remember that `x` is fixed, so $S_{xx}$ will be the same for each interval.
- You could, but do not need to write a `for` loop. Remember vectorized operations.

```{r eval=TRUE}
crit_90
lower_90[i]
upper_90[i]
```

**(c)** What proportion of these intervals contain the true value of $\beta_0$?

**(d)** Based on these intervals, what proportion of the simulations would reject the test $H_0: \beta_0 = 0$ vs $H_1: \beta_0 \neq 0$ at $\alpha = 0.10$?
```{r eval=TRUE}
(t_90 = beta_hat_0[i] - 0 / sqrt(( 1 / n) + mean(x)^2 / Sxx))
```


**(e)** For each of the $\hat{\beta}_0$ that you simulated calculate a 99% confidence interval. Store the lower limits in a vector `lower_99` and the upper limits in a vector `upper_99`.
```{r eval=TRUE}
crit_99
lower_99[i]
upper_99[i]
```

**(f)** What proportion of these intervals contain the true value of $\beta_0$?
```{r eval=TRUE}

```

**(g)** Based on these intervals, what proportion of the simulations would reject the test $H_0: \beta_0 = 0$ vs $H_1: \beta_0 \neq 0$ at $\alpha = 0.01$?
```{r eval=TRUE}
(t_99 = beta_hat_0[i] - 0 / sqrt(( 1 / n) + mean(x)^2 / Sxx))

```



